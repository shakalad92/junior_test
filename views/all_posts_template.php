
<div class="row">
<?php foreach ($products as $product): ?>
    <div class="col-3 mt-4">
        <div class="card text-center text-white bg-dark">
        <div class="form-check-inline">
            <label class="form-check-label ml-4 mt-3" for="<?=$product['id']?>">
                <input type="checkbox" class="form-check-input" id="<?=$product['id']?>">Delete?</label>
        </div>
        <h5 class="card-header"><?=$product['sku']?></h5>
            <div class="card-body">
                <h5 class="card-title"><?=$product['name']?></h5>
                <p><?=$product['price']?></p>
                <?php if ($product['product_type'] == 'dvd'): ?>
                    <p>Size: <?=$product['size'];?></p>
                <?php elseif ($product['product_type'] == 'book'): ?>
                    <p>Weight: <?=$product['weight'];?></p>
                <?php else: ?>
                    <p>Dimension: <?=$product['height'];?>x<?=$product['width'];?>x<?=$product['length'];?></p>
                <?php endif;?>
            </div>
        </div>
    </div>
<?php endforeach;?>
</div>

<script>
    $(document).ready(function(){
        $("#m_delete").click(function(){        
            var checked = [];
            if($('input:checkbox:checked').length > 0) {
                $('input:checkbox:checked').each(function() {
                    checked.push($(this).attr('id'));
                });
                sendInputs(checked);
            } else {
                alert("There's nothing to delete");
            }
        });

        function sendInputs(inputs) {
            $.ajax({
                type : "POST",
                url: "Product/delete",
                data: {'data': inputs},
                success: function(response) {
                    location.reload();
                },
                error: function(errResponse) {
                    alert(errResponse);
                }
            });
        }
    });
</script>


