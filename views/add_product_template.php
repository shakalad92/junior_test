<form method="POST" class="mb-5 col-md-5" id="ss" action="/test.loc/DVD/add">

    <div class="form-group">
        <label for="sku">SKU</label>
        <input type="text" class="form-control text-white bg-dark" id="sku" name="sku" placeholder="JVC200123" value="<?= isset($_POST['sku']) ? $_POST['sku'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['sku']) ? $errors['sku'] : ""; ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control text-white bg-dark" id="name" name="name" placeholder="Prince of Persia" value="<?= isset($_POST['name']) ? $_POST['name'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['name']) ? $errors['name'] : ""; ?>
        </div>
    </div>
    <div class="form-group">
        <label for="price">Price ($)</label>
        <input type="text" class="form-control text-white bg-dark" id="price" name="price" placeholder="17 $" value="<?= isset($_POST['price']) ? $_POST['price'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['price']) ? $errors['price'] : ""; ?>
        </div>
    </div>
    <div class="form-group">
        <label for="product_type">Product Type:</label>
        <select id="product_type" class="form-control  text-white bg-dark" name="product_type">
            <option selected value="dvd" name="dvd">DVD</option>
            <option value="book" name="book">Book</option>
            <option value="furniture" name="furniture">Furniture</option>
        </select>
    </div>
    <div class="form-group" id="fSize">
        <label for="size">Size (MB)</label>
        <input type="text" class="form-control text-white bg-dark" id="size" name="size" placeholder="1024 Mb" value="<?= isset($_POST['size']) ? $_POST['size'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['size']) ? $errors['size'] : ""; ?>
        </div>
    </div>
    <div class="form-group" id="fWeight">
        <label for="weight">Weight (Kg)</label>
        <input type="text" class="form-control text-white bg-dark" id="weight" name="weight" placeholder="3 kg" value="<?= isset($_POST['weight']) ? $_POST['weight'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['weight']) ? $errors['weight'] : ""; ?>
        </div>
    </div>
    <div class="form-group" id="fFurniture">
        <label for="height">Height (Cm)</label>
        <input type="text" class="form-control text-white bg-dark" id="height" name="height" placeholder="3" value="<?= isset($_POST['height']) ? $_POST['height'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['height']) ? $errors['height'] : ""; ?>
        </div>
        <label for="width">Width (Cm)</label>
        <input type="text" class="form-control text-white bg-dark" id="width" name="width" placeholder="5" value="<?= isset($_POST['width']) ? $_POST['width'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['width']) ? $errors['width'] : ""; ?>
        </div>
        <label for="length">Length (Cm)</label>
        <input type="text" class="form-control text-white bg-dark" id="length" name="length" placeholder="5" value="<?= isset($_POST['length']) ? $_POST['length'] : ""; ?>">
        <div class="error mt-1">
            <?= isset($errors['length']) ? $errors['length'] : ""; ?>
        </div>
    </div>

    <button type="submit" class="btn btn-dark">Add +</button>
</form>

<script>
    $("#fWeight").hide();
    $("#fFurniture").hide();
    $('select').on('change', function () {
        $("#size").val("");
        $("#weight").val("");
        $("#furniture").val("");
        if (this.value == "book") {
            $("#ss").attr('action', '/test.loc/Book/add');
            $("#fSize").hide();
            $("#fWeight").show();
            $("#fFurniture").hide();
        }
        if (this.value == 'dvd') {
            $("#ss").attr('action', '/test.loc/DVD/add');
            $("#fSize").show();
            $("#fWeight").hide();
            $("#fFurniture").hide();
        }
        if (this.value == 'furniture') {
            $("#ss").attr('action', '/test.loc/Furniture/add');
            $("#fSize").hide();
            $("#fWeight").hide();
            $("#fFurniture").show();
        }
    });

    $("#save").click(function(){        
        $("#ss").submit(); // Submit the form
    });
</script>
