<?php

use core\Request;
use core\Validator;

spl_autoload_register(function ($class) {
    require_once __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $class) . ".php";
});

function debug($arr)
{
    echo "<pre>" . print_r($arr, 1) . "</pre>";
    die;
}

$request = new Request($_GET, $_POST, $_SERVER);
$validator = new Validator();

$uri = $_SERVER['REQUEST_URI'];
$uri = explode('/', $uri);
unset($uri[0]);
$uri = array_values($uri);

$controller = isset($uri[1]) && $uri[1] !== '' ? $uri[1] : ucfirst('product');
$action = isset($uri[2]) && $uri[2] !== '' ? $uri[2] : 'index';

$controller = sprintf("controllers\%sController", $controller);
$action = sprintf("%sAction", $action);

$controller = new $controller($request);

try {
    $controller->$action();
} catch (\Exception $event) {
    $controller->errorHandler($event->getMessage());
}

$controller->render();
