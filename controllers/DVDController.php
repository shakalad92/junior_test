<?php

namespace controllers;

use core\DBConnect;
use core\DBDriver;
use core\exception\ModelException;
use core\Validator;
use models\DVDModel;

class DVDController extends BaseController
{
    public function addAction()
    {
        if ($this->request->isPost()) {
            try {
                $mDVD = new DVDModel(new DBDriver(DBConnect::getConnect()), new Validator());
                $mDVD->add([
                    'sku' => $this->request->post('sku'),
                    'name' => $this->request->post('name'),
                    'price' => $this->request->post('price'),
                    'product_type' => $this->request->post('product_type'),
                    'size' => $this->request->post('size'),
                    'weight' => "",
                    'height' => "",
                    'width' => "",
                    'length' => "",
                ]);

                $this->redirect("/test.loc");
            } catch (ModelException $event) {
                $this->content = $this->build(__DIR__ . "/../views/add_product_template.php", ['errors' => $event->getErrors()]);
            }
        }
    }
}
