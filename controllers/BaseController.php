<?php

namespace controllers;

use core\Request;

class BaseController
{
    protected $title = "Base Controller";
    protected $content = "";
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function errorHandler($message)
    {
        $this->content = $message;
    }

    public function render()
    {
        echo $this->build(__DIR__ . "/../views/base_template.php", [
            'title' => $this->title,
            'content' => $this->content,
        ]);
    }

    protected function build($template, array $params = [])
    {
        ob_start();
        extract($params);
        require_once $template;

        return ob_get_clean();
    }

    protected function redirect($uri)
    {
        header(sprintf('Location: %s', $uri));
        die;
    }
}
