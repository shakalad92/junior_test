<?php

namespace controllers;

use core\DBConnect;
use core\DBDriver;
use core\exception\ModelException;
use core\Validator;
use models\BookModel;

class BookController extends BaseController
{
    public function addAction()
    {
        if ($this->request->isPost()) {
            try {
                $mBook = new BookModel(new DBDriver(DBConnect::getConnect()), new Validator());
                $mBook->add([
                    'sku' => $this->request->post('sku'),
                    'name' => $this->request->post('name'),
                    'price' => $this->request->post('price'),
                    'product_type' => $this->request->post('product_type'),
                    'size' => "",
                    'weight' => $this->request->post('weight'),
                    'height' => "",
                    'width' => "",
                    'length' => "",
                ]);

                $this->redirect("/test.loc");
            } catch (ModelException $event) {
                $this->content = $this->build(__DIR__ . "/../views/add_product_template.php", ["errors" => $event->getErrors()]);
            }
        }
    }
}
