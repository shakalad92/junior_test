<?php

namespace controllers;

use core\DBConnect;
use core\DBDriver;
use core\Validator;
use models\ProductModel;

class ProductController extends BaseController
{
    public function indexAction()
    {
        $this->title = "Product List";
        $mProduct = new ProductModel(new DBDriver(DBConnect::getConnect()), new Validator());
        $this->content = $this->build(__DIR__ . "/../views/all_posts_template.php", ['products' => $mProduct->getAll()]);
    }

    public function addAction()
    {
        if ($this->request->isGet()) {
            $this->title = "Product Add";
            $this->content = $this->build(__DIR__ . "/../views/add_product_template.php");
        }
    }

    public function deleteAction()
    {
        $product_model = new ProductModel(new DBDriver(DBConnect::getConnect()), new Validator ());
        $product_model->deleteAll($this->request->post('data'));

        $this->redirect('/test.loc/');
    }
}
