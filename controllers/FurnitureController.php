<?php

namespace controllers;

use core\DBConnect;
use core\DBDriver;
use core\exception\ModelException;
use core\Validator;
use models\FurnitureModel;

class FurnitureController extends BaseController
{
    public function addAction()
    {

        if ($this->request->isPost()) {
            try {
                $mFurniture = new FurnitureModel(new DBDriver(DBConnect::getConnect()), new Validator());
                $mFurniture->add([
                    'sku' => $this->request->post('sku'),
                    'name' => $this->request->post('name'),
                    'price' => $this->request->post('price'),
                    'product_type' => $this->request->post('product_type'),
                    'size' => "",
                    'weight' => "",
                    'height' => $this->request->post('height'),
                    'width' => $this->request->post('width'),
                    'length' => $this->request->post('length'),
                ]);

                $this->redirect("/test.loc");
            } catch (ModelException $event) {
                $this->content = $this->build(__DIR__ . "/../views/add_product_template.php", ['errors' => $event->getErrors()]);
            }
        }
    }
}
