<?php

namespace models;

use core\DBDriver;
use core\Validator;

class DVDModel extends ProductModel
{
    protected $schema = [
        'sku' => [
            'unique' => true,
            'not_blank' => true,
        ],
        'name' => [
            'not_blank' => true,
        ],
        'price' => [
            'not_blank' => true,
        ],
        'product_type' => [
            'not_blank' => true,
        ],
        'size' => [
            'not_blank' => true,
        ], 
        'weight' => [
            'blank' => true,
        ],
        'height' => [
            'blank' => true,
        ],
        'width' => [
            'blank' => true,
        ],
        'length' => [
            'blank' => true,
        ],
    ];

    public function __construct(DBDriver $pdo, Validator $validator)
    {
        parent::__construct($pdo, $validator);
        $this->validator->setRules($this->schema);
    }
}
