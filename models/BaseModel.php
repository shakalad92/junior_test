<?php

namespace models;

use core\DBDriver;
use core\Validator;

abstract class BaseModel
{
    protected $pdo;
    protected $table;
    protected $validator;

    public function __construct(DBDriver $pdo, Validator $validator,$table)
    {
        $this->pdo = $pdo;
        $this->table = $table;
        $this->validator = $validator;
    }

    public function getAll()
    {
        $sql = sprintf("SELECT * FROM %s", $this->table);
        return $this->pdo->select($sql);
    }

    public function getById($id)
    {
        $sql = sprintf("SELECT * FROM %s WHERE id = :id", $this->table);
        return $this->pdo->select($sql, ['id' => $id], DBDriver::FETCH_ONE);
    }

    public function deleteAll($data)
    {
        $sql = "DELETE FROM $this->table WHERE id = :id";
        return $this->pdo->delete($sql, $data);
    }
}
