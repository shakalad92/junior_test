<?php

namespace models;

use core\DBDriver;
use core\exception\ModelException;
use core\Validator;

class ProductModel extends BaseModel
{
    public function __construct(DBDriver $pdo, Validator $validator)
    {
        parent::__construct($pdo, $validator, 'products');
    }

    public function add(array $user_data)
    {
        $this->validator->execute($user_data);

        if (!$this->validator->success) {
            throw new ModelException($this->validator->errors);
        }

        $this->pdo->insert($this->validator->cleaned_data, $this->table);
    }
}
