<?php

namespace models;

use core\Validator;
use core\DBDriver;

class FurnitureModel extends ProductModel
{
    protected $schema = [
        'sku' => [
            'unique' => true,
            'not_blank' => true,
        ],
        'name' => [
            'not_blank' => true,
        ],
        'price' => [
            'not_blank' => true,
        ],
        'product_type' => [
            'not_blank' => true,
        ],
        'size' => [
            'blank' => true,
        ], 
        'weight' => [
            'blank' => true,
        ],
        'height' => [
            'not_blank' => true,
        ],
        'width' => [
            'not_blank' => true,
        ],
        'length' => [
            'not_blank' => true,
        ],
    ];

    public function __construct(DBDriver $pdo, Validator $validator)
    {
        parent::__construct($pdo, $validator);
        $this->validator->setRules($this->schema);
    }
}
