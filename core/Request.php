<?php

namespace core;

class Request
{
    const REQUEST_POST = 'POST';
    const REQUEST_GET = 'GET';

    private $get;
    private $post;
    private $server;

    public function __construct($get, $post, $server)
    {
        $this->get = $get;
        $this->post = $post;
        $this->server = $server;
    }

    public function isPost()
    {
        return $this->server['REQUEST_METHOD'] === self::REQUEST_POST;
    }

    public function isGet()
    {
        return $this->server['REQUEST_METHOD'] === self::REQUEST_GET;
    }

    public function post($key = null)
    {
        return $this->getArr($this->post, $key);
    }

    public function server($key = null)
    {
        return $this->getArr($this->server, $key);
    }

    public function getArr(array $arr, $key = null)
    {
        if (!$key) {
            return $arr;
        }
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        return null;
    }
}
