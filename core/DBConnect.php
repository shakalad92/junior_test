<?php

namespace core;

class DBConnect
{
    private static $instance;

    public static function getConnect()
    {
        if (self::$instance === null) {
            self::$instance = self::getPDO();
        }
        return self::$instance;
    }

    private static function getPDO()
    {
        $dsn = sprintf('%s:host=%s;dbname=%s', 'mysql', 'localhost', 'junior_test');
        return new \PDO($dsn, 'root', '');
    }
}
