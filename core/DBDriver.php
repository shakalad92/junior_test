<?php

namespace core;

use core\exception\ModelException;

class DBDriver
{
    const FETCH_ALL = 'all';
    const FETCH_ONE = 'one';

    protected $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function select($sql, array $params = [], $fetch = self::FETCH_ALL)
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

        return $fetch === self::FETCH_ALL ? $stmt->fetchAll() : $stmt->fetch();
    }

    public function insert(array $params, $table)
    {
        $col = sprintf('(%s)', implode(', ', array_keys($params)));
        $mask = sprintf('(:%s)', implode(', :', array_keys($params)));

        $sql = sprintf("INSERT INTO %s %s VALUES %s", $table, $col, $mask);
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
    }

    public function delete($sql, array $data)
    {
        foreach($data as $sku) {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(['id'=>$sku]);
        }
    }
}
