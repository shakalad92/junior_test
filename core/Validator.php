<?php

namespace core;

use Exception;

class Validator
{
    public $cleaned_data = [];
    public $errors = [];
    public $success = false;
    protected $rules;

    public function __construct()
    {
        $this->pdo = DBConnect::getConnect();
    }

    public function execute(array $user_data)
    {
        if (!$this->rules) {
            throw new \Exception("No rules for validator!");
        }

        foreach ($this->rules as $field => $rule) {
            if ($user_data[$field] == '' && isset($rule['not_blank'])) {
                $this->errors[$field] = "Field $field must not be blank";
            } elseif ($user_data[$field] !== '' && isset($rule['blank'])) {
                $user_data[$field] = '';
            }

            if ($this->isNotUnique($user_data[$field]) && isset($rule['unique'])) {
                $this->errors[$field] = "Value $user_data[$field] is allready used";
            }

            if (empty($this->errors[$field])) {
                $this->cleaned_data[$field] = trim(strip_tags($user_data[$field]));
            }
        }

        if (empty($this->errors)) {
            $this->success = true;
        }
    }

    public function isNotUnique($sku)
    {
        $sql = 'SELECT count(*) FROM products WHERE sku = :sku';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(['sku' => $sku]);
        $sku = $stmt->fetchColumn();

        return $sku;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }
}
